# Laravel Tagged Cache

##Install

```bash
composer require domatskiy/laravel-tagged-cache
```

Publish needed
```bash
php artisan vendor:publish --provider="Domatskiy\TaggedCache\TaggedCacheServiceProvider"
```

Add information about the provider to the config/app.php => providers

```php
Domatskiy\TaggedCache\HtmlCacheServiceProvider::class
```  