<?php

namespace Domatskiy\TaggedCache;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Event;

class TaggedCacheServiceProvider extends ServiceProvider
{
    /**
     * Bootstrapping the package.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/config/tagged-cache.php' => config_path('tagged-cache.php'),
        ], 'cache');


        $this->loadMigrationsFrom(__DIR__.'/migrations');

        Event::subscribe(\Domatskiy\TaggedCache\Listeners\CacheEventSubscriber::class);

        Event::listen('cache:cleared', function () {

            /**
             * очистка базы тегированного кэша
             */
            CacheTags::truncateBase();
        });

    }

    /**
     * Bind some Interfaces and implementations.
     */
    protected function bootBindings()
    {
        /*$this->app->singleton('HtmlCacheStorage', function ($app) {
            return new \Domatskiy\HtmlCache\HtmlStorage();
        });*/

    }
}
