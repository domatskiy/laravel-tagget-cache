<?php

namespace Domatskiy\TaggedCache\Test;

use Carbon\Carbon;

use Domatskiy\TaggedCache\CacheTags;
use Domatskiy\TaggedCache\TaggedCacheServiceProvider;

use Orchestra\Testbench\TestCase as TestCase;


class TaggedCacheTest extends TestCase
{
    protected function getPackageProviders($app)
    {
        return [TaggedCacheServiceProvider::class];
    }

    protected function getPackageAliases($app)
    {
        return [
            'HtmlStorage' => CacheTags::class,
            'DBCacheKey' => \Domatskiy\TaggedCache\DB\CacheKey::class,
            'DBCacheKeyTag' => \Domatskiy\TaggedCache\DB\CacheKeyTag::class,
        ];
    }

    /**
     * Define environment setup.
     *
     * @param  \Illuminate\Foundation\Application  $app
     * @return void
     */
    protected function getEnvironmentSetUp($app)
    {
        // Setup default database to use sqlite :memory:
        // $app['config']->set('tagged-cache.path', '');

        $app['config']->set('database.default', 'testing');
        $app['config']->set('database.connections.testing', [
            'driver'   => 'sqlite',
            'database' => ':memory:',
            'prefix'   => '',
        ]);
    }

    /**
     * Resolve application Console Kernel implementation.
     *
     * @param  \Illuminate\Foundation\Application  $app
     * @return void
     */
    protected function resolveApplicationConsoleKernel($app)
    {
        $app->singleton('Illuminate\Contracts\Console\Kernel', '\Orchestra\Testbench\Console\Kernel');
    }

    public function setUp()
    {
        parent::setUp();

        $this->loadMigrationsFrom(__DIR__ . '/../src/migrations');
        $this->loadLaravelMigrations(['--database' => 'testing']);
    }

    public function testMisc()
    {

    }

    /** @test */
    public function it_runs_the_migrations()
    {
        $tested_key = 'test-key';
        $now = Carbon::now();

        $res = \DB::table('tagged_cache')->insert([
            'key' => $tested_key,
            'created_at' => $now,
            'updated_at' => $now,
        ]);

        # $this->assertEquals($tested_key, $htmlCache->path, 'path not found');

    }

}
