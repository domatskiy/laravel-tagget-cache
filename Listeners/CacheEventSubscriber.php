<?php

namespace Domatskiy\TaggedCache\Listeners;

use Illuminate\Cache\Events\KeyForgotten;
use Illuminate\Cache\Events\KeyWritten;
use Illuminate\Cache\Events\CacheHit;

class CacheEventSubscriber
{
    #public function onKeyWritten(KeyWritten $event) {}
    #public function CacheHit(CacheHit $event){}

    public function onKeyForgotten(KeyForgotten $event) {

        // очистка базы тегированного кэша по ключу основного кэша
        \Domatskiy\TaggedCache\CacheTags::clearBaseByKey($event->key);

    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  \Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        /*$events->listen(
            'Illuminate\Cache\Events\KeyWritten',
            'App\Listeners\CacheEventSubscriber@onKeyWritten'
        );

        $events->listen(
            'Illuminate\Cache\Events\CacheHit',
            'App\Listeners\CacheEventSubscriber@CacheHit'
        );*/

        $events->listen(
            'Illuminate\Cache\Events\KeyForgotten',
            'Domatskiy\TaggedCache\Listeners\CacheEventSubscriber@onKeyForgotten'
        );

    }

}