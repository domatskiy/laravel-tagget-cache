<?php

namespace Domatskiy\TaggedCache\DB;

use Illuminate\Database\Eloquent\Model;

/**
 * \Domatskiy\TaggedCache\DB
 *
 * @property int $id
 * @property string $cache_id
 * @property string $tag
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\Domatskiy\TaggedCache\DB\CacheKey whereCacheId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Domatskiy\TaggedCache\DB\CacheKey whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Domatskiy\TaggedCache\DB\CacheKey whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Domatskiy\TaggedCache\DB\CacheKey whereTag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Domatskiy\TaggedCache\DB\CacheKey whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CacheKey extends Model
{
    /**
     * Таблица БД, используемая моделью.
     * @var string
     */
    protected $table = 'tagged_cache';

    /**
     * Атрибуты, исключенные из JSON-представления модели.
     *
     * @var array
     */
    protected $hidden = array();

    protected $fillable = array('key');
}