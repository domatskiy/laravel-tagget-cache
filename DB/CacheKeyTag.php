<?php

namespace Domatskiy\TaggedCache\DB;

use Illuminate\Database\Eloquent\Model;

/**
 * Domatskiy\TaggedCache\DB\CacheKeyTag
 *
 * @property int $id
 * @property string $cache_id
 * @property string $tag
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\Domatskiy\TaggedCache\DB whereCacheId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Domatskiy\TaggedCache\DB whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Domatskiy\TaggedCache\DB whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Domatskiy\TaggedCache\DB whereTag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Domatskiy\TaggedCache\DB whereUpdatedAt($value)
 * @mixin \Eloquent
*/

class CacheKeyTag extends Model
{
    /**
     * Таблица БД, используемая моделью.
     * @var string
     */
    protected $table = 'tagged_cache_tags';

    /**
     * Атрибуты, исключенные из JSON-представления модели.
     *
     * @var array
     */
    protected $hidden = array();

    protected $fillable = array('cache_id', 'tag');

    protected $guarded = array('id');

}