<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCacheTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tagged_cache_tags', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('cache_id');
            $table->string('tag', 255);
            $table->timestamps();

            $table->foreign('cache_id')
                ->references('id')
                ->on('cache')
                ->onDelete('cascade');

        });

        Schema::table('cache', function(Blueprint $table){

            $table->renameColumn('cache_id', 'key');
            $table->dropColumn('tag');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cache_tags');

        Schema::table('cache', function(Blueprint $table){

            $table->renameColumn('key', 'cache_id');

            $table
                ->string('tag')
                ->after('cache_id');

        });
    }
}
